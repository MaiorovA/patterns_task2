using System;
using System.Collections.Generic;

interface IProduct
{
    void Accept(List<IProductVisitor> visitors);
}

class Book : IProduct
{
    private string _title;
    private string _author;
    private decimal _price;

    public Book(string title, string author, decimal price)
    {
        _title = title;
        _author = author;
        _price = price;
    }

    public void Accept(List<IProductVisitor> visitors)
    {
        foreach (var visitor in visitors)
        {
            visitor.Visit(this);
        }
    }

    public string GetTitle()
    {
        return _title;
    }

    public string GetAuthor()
    {
        return _author;
    }

    public decimal GetPrice()
    {
        return _price;
    }
}

class MobilePhone : IProduct
{
    private string _brand;
    private string _model;
    private decimal _price;

    public MobilePhone(string brand, string model, decimal price)
    {
        _brand = brand;
        _model = model;
        _price = price;
    }

    public void Accept(List<IProductVisitor> visitors)
    {
        foreach (var visitor in visitors)
        {
            visitor.Visit(this);
        }
    }

    public string GetBrand()
    {
        return _brand;
    }

    public string GetModel()
    {
        return _model;
    }

    public decimal GetPrice()
    {
        return _price;
    }
}

interface IProductVisitor
{
    void Visit(Book book);
    void Visit(MobilePhone mobilePhone);
}

class CitizenVisitor : IProductVisitor
{
    public void Visit(Book book)
    {
        Console.WriteLine($"Processing a book '{book.GetTitle()}' by {book.GetAuthor()}, price: {book.GetPrice()} for a citizen");
    }

    public void Visit(MobilePhone mobilePhone)
    {
        Console.WriteLine($"Processing a mobile phone {mobilePhone.GetBrand()} {mobilePhone.GetModel()}, price: {mobilePhone.GetPrice()} for a citizen");
    }
}

class EmployeeVisitor : IProductVisitor
{
    public void Visit(Book book)
    {
        Console.WriteLine($"Processing a book '{book.GetTitle()}' by {book.GetAuthor()}, price: {book.GetPrice()} for an employee");
    }

    public void Visit(MobilePhone mobilePhone)
    {
        Console.WriteLine($"Processing a mobile phone {mobilePhone.GetBrand()} {mobilePhone.GetModel()}, price: {mobilePhone.GetPrice()} for an employee");
    }
}

abstract class ProductCreator
{
    public abstract IProduct CreateProduct(params object[] parameters);
}

class BookCreator : ProductCreator
{
    public override IProduct CreateProduct(params object[] parameters)
    {
        if (parameters.Length == 3 && parameters[0] is string && parameters[1] is string && parameters[2] is decimal)
        {
            return new Book((string)parameters[0], (string)parameters[1], (decimal)parameters[2]);
        }
        return null;
    }
}

class MobilePhoneCreator : ProductCreator
{
    public override IProduct CreateProduct(params object[] parameters)
    {
        if (parameters.Length == 3 && parameters[0] is string && parameters[1] is string && parameters[2] is decimal)
        {
            return new MobilePhone((string)parameters[0], (string)parameters[1], (decimal)parameters[2]);
        }
        return null;
    }
}

class Program
{
    static void Main(string[] args)
    {
        List<IProduct> products = new List<IProduct>();
        ProductCreator bookCreator = new BookCreator();
        ProductCreator mobilePhoneCreator = new MobilePhoneCreator();
        products.Add(bookCreator.CreateProduct("The Lord of the Rings", "J.R.R. Tolkien", 19.99m));
        products.Add(mobilePhoneCreator.CreateProduct("Apple", "iPhone 11", 699m));

        List<IProductVisitor> visitors = new List<IProductVisitor>();
        visitors.Add(new CitizenVisitor());
        visitors.Add(new EmployeeVisitor());

        foreach (var product in products)
        {
            product.Accept(visitors);
        }
    }
}
